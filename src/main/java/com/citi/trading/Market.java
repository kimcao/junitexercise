package com.citi.trading;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * Client component for a stock market.
 * Uses JMS to send trade orders and to receive notifications.
 * 
 * @author Will Provost
 */
public class Market implements MessageListener, OrderPlacer {

    private static Logger LOGGER = Logger.getLogger (Market.class.getName ());
    
    public static final String ID_PREFIX = "Order";
    public static final int TIMEOUT_MSEC = 15000;
    public static final int MONITOR_MSEC = 5000;
    	
	private JAXBContext jaxbContext;
	
	private QueueConnectionFactory factory;
	private Queue queue;
	private Queue replyQueue;
	
	private int nextID = 1;
	private Map<Integer,Consumer<Trade>> callbacks = new HashMap<>();
	private Map<Integer,Trade> orders = new HashMap<>();
	
	/**
	 * Sets up a JAXB context that will be used to translate {@link Trade}s
	 * to and from the XML representation used by the mock market.
	 */
	public Market() {
        try {
            jaxbContext = JAXBContext.newInstance (Trade.class);

	        Properties props = new Properties();
	        props.setProperty (Context.INITIAL_CONTEXT_FACTORY,
	            "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
	        props.setProperty (Context.PROVIDER_URL, "tcp://will1.conygre.com:61616");
            Context context = new InitialContext (props);
            factory = (QueueConnectionFactory) context.lookup("ConnectionFactory");
            queue = (Queue) context.lookup ("dynamicQueues/OrderBroker");

			//INTENTIONAL COMPILE ERROR HERE to remind you to fix this URL:
            replyQueue = (Queue) context.lookup ("dynamicQueues/YuntingCao");
            
            QueueConnection connection = factory.createQueueConnection ();
            QueueSession session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        	connection.start();
        	
        	QueueReceiver receiver = session.createReceiver(replyQueue);
        	receiver.setMessageListener(this);
        	
        	LOGGER.info("Listening for trade confirmations.");
            
        } catch (Exception ex) {
            LOGGER.log (Level.SEVERE, "Couldn't create JAXB context!", ex);
        }
	}
	
	/**
	 * Translates the given trade to an XML string, and sends a JMS TextMessage
	 * with that string as its body to the configured JMS queue. Sets a JMS
	 * correlation ID to be used in matching trade notifications when they come
	 * back to us, and registers the provided callback under that ID.
	 * 
	 * @param A trade to be executed on the mock market
	 * @param An object that can be called later with the trade confirmation
	 */
	public synchronized void placeOrder(Trade order, Consumer<Trade> callback) {
		try ( StringWriter out = new StringWriter(); ) {
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.marshal(order, out);
			String body = out.toString();
			int orderID = nextID++;
			
			QueueConnection connection = null;
			QueueSession session = null;
            try {
            	connection = factory.createQueueConnection ();
            	session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
	            QueueSender sender = session.createSender(queue);
				
				TextMessage message = session.createTextMessage(body);
				message.setJMSReplyTo(replyQueue);
				message.setJMSCorrelationID(ID_PREFIX + orderID);
				sender.send(message);
	
				callbacks.put(orderID, callback);
				orders.put(orderID, order);
				
				LOGGER.info("Placed order: " + order);
            } finally {
            	session.close();
            	connection.close();
            }
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Couldn't place order.", ex);
		}
	}
	
	/**
	 * JMS listener for trade notifications. Translates the message back to a
	 * {@link Trade} object, which will have its {@link Trade#getResult result}
	 * property filled in with the status of the order. Looks up the registered 
	 * callback by the JMS correlation ID, calls that object, and removes it
	 * from the callback registry.
	 */
	public synchronized void onMessage(Message message) {
		try {
			TextMessage textMessage = (TextMessage) message;
			String body = textMessage.getText();
			
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			Trade notification = (Trade) unmarshaller.unmarshal(new StringReader(body));
			LOGGER.info("Received notification: " + notification);

			int orderID = Integer.parseInt
					(message.getJMSCorrelationID().replace(ID_PREFIX, ""));
			Consumer<Trade> callback = callbacks.get(orderID);
			if (callback != null) {
				callback.accept(notification);
				callbacks.remove(orderID);
				orders.remove(orderID);
			} else {
				LOGGER.warning("No callback found for order " + orderID);
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Couldn't process trade notification.", ex);
		}
	}
}
