import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.closeTo;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.junit.Test;

import com.citi.trading.Investor;
import com.citi.trading.OrderPlacer;
import com.citi.trading.Trade;


public class InvestorTest {
	
	private static final int STARTING_CASH = 20000;
	
	public static class MockMarket implements OrderPlacer {
		private Map<Integer,Consumer<Trade>> callbacks = new HashMap<>();
		private Map<Integer,Trade> orders = new HashMap<>();
		private int nextID = 1;
		
		public void placeOrder(Trade order, Consumer<Trade> callback) {
			int orderID = nextID++;
			callbacks.put(orderID, callback);
			orders.put(orderID, order);
			System.out.println("Placed order: " + order);
			callback.accept(order);
		}
	}
	
	
	@Test
	public void testBuy() {	
		Investor investor = new Investor(STARTING_CASH);
		investor.setMarket(new MockMarket());
		investor.buy("MRK", 100, 60);
		investor.buy("MRK", 50, 100);
		
		double remainingCash = investor.getCash();
		Map<String, Integer> expectedProfolio = new HashMap<String, Integer>();
		expectedProfolio.put("MRK", 150);
		assertThat(remainingCash,equalTo(STARTING_CASH-100*60-50*100.0));
		assertThat(investor.getPortfolio(),equalTo(expectedProfolio));

	}
	
	@Test
	public void testSell() {		
		Map<String, Integer> profolio = new HashMap<String, Integer>();
		profolio.put("MRK", 150);
		Investor investor = new Investor(profolio, STARTING_CASH);
		investor.setMarket(new MockMarket());
		
		investor.sell("MRK", 50, 110);
		
		Map<String, Integer> expectedProfolio = new HashMap<String, Integer>();
		expectedProfolio.put("MRK", 100);
		assertThat(STARTING_CASH+50*110.0,equalTo(investor.getCash()));
		assertThat(investor.getPortfolio(),equalTo(expectedProfolio));

	}
	
	

}